﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

	public GameObject playBtn;

	public GameObject difficultyPanel;
	private Animator difficultyPanelAnimator;

	// Use this for initialization
	void Start () {
		difficultyPanelAnimator = difficultyPanel.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenDifficulties() {
		playBtn.SetActive (false);
		difficultyPanelAnimator.Play ("show");
	}

	public void HideDifficulties() {
		if (difficultyPanelAnimator.GetCurrentAnimatorStateInfo (0).IsName ("shown")) {
			playBtn.SetActive (true);
			difficultyPanelAnimator.Play ("hide");
		}
	}

	public void EasyDifficultySelected() {
		LoadLevelWithDifficulty (0);
	}

	public void MediumDifficultySelected() {
		LoadLevelWithDifficulty (1);
	}

	public void InvincibleDifficultySelected() {
		LoadLevelWithDifficulty (2);
	}

	private void LoadLevelWithDifficulty(int difficulty) {
		PlayerPrefs.SetInt ("Difficulty", difficulty);
		PlayerPrefs.Save ();

		Application.LoadLevel("game");
	}
}
