﻿using UnityEngine;
using System.Collections;

public class GameEvent : MonoBehaviour {

	public const string GAME_OVER = "gameOver";

	public const string PLAY = "play";

	public const string UPDATE_SCORE = "updateScore";

	public const string PLAYER_CELL_SELECTED = "playerCellSelected";
	
	public const string AI_CELL_SELECTED = "aiCellSelected";

}
