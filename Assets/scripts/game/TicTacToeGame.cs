﻿using UnityEngine;
using System.Collections;

public class TicTacToeGame : MonoBehaviour {

	public GameFieldController controller;
	public UIController uiController;

	public GameModel model;

	public AIController aiController;
	public AIModel aiModel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Notify (string eventType, Object target, params object[] data)
	{
		INotifiable[] listControllers = GetNotifiableControllers();
		foreach(INotifiable c in listControllers)
		{
			c.OnNotification(eventType, target, data);
		}
	}

	public INotifiable[] GetNotifiableControllers() {
		return new INotifiable[]{controller, uiController};
	}
}
