﻿using UnityEngine;
using System.Collections;

public class GameModel : GameElement {

	public static Color PlayersColor = new Color(98f/255f, 135f/255f, 208f/255f);

	public static Color AIColor = new Color(208f/255f, 98f/255f, 98f/255f);

	public static Color DefaultColor = Color.white;

	public enum GameState
	{
		PlayerStep,
		AIStep,
		AIStepDelay, // delay before AI step for realism
		PlayerWins,
		AIWins,
		NoOneWins
	}

	public GameState currentGameState;

	public Sprite crossSprite;
	public Sprite circleSprite;
	
	public Cell[,] gridCells = new Cell[3,3]; 

	public int winCount;
	public int failCount;
	public int drawCount;

	public bool playerBeginFirst;

	public float stepDelay; // delay after player's step
	public float stepDelayTimer; // timer of steps delay

	public int matchSteps; // total number of match steps

	public void LoadPlayerPrefs() {
		if (PlayerPrefs.HasKey ("Difficulty")) {
			int difficulty = PlayerPrefs.GetInt("Difficulty");
			switch(difficulty) {
				case 0:
					game.aiModel.aiDifficulty = AIModel.AIDifficulty.Easy;
					break;
				case 1:
					game.aiModel.aiDifficulty = AIModel.AIDifficulty.Medium;
					break;
				case 2:
					game.aiModel.aiDifficulty = AIModel.AIDifficulty.Invincible;
					break;
				default:
					game.aiModel.aiDifficulty = AIModel.AIDifficulty.Easy;
					break;
			}
		}

		if (PlayerPrefs.HasKey ("WinCount")) {
			winCount = PlayerPrefs.GetInt ("WinCount");
		}

		if (PlayerPrefs.HasKey ("FailCount")) {
			failCount = PlayerPrefs.GetInt ("FailCount");
		}

		if (PlayerPrefs.HasKey ("DrawCount")) {
			drawCount = PlayerPrefs.GetInt ("DrawCount");
		}
	}

	public void SavePlayerPrefs() {
		PlayerPrefs.SetInt ("WinCount", winCount);
		PlayerPrefs.SetInt ("FailCount", failCount);
		PlayerPrefs.SetInt ("DrawCount", drawCount);
		PlayerPrefs.Save ();
	}

}


