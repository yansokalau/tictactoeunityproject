﻿using UnityEngine;
using System.Collections;

public class AIModel : GameElement {

	public enum AIDifficulty
	{
		Easy, Medium, Invincible
	}

	public AIDifficulty aiDifficulty;
}
