﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : GameElement {

	public void DoStep() {

		switch (game.aiModel.aiDifficulty) {
			case AIModel.AIDifficulty.Easy:

				DoRandomStep ();

				break;
			case AIModel.AIDifficulty.Medium:
					
					// check combos in 50% cases for this difficulty
					if (Random.value < 0.5f && DoComboCheck (checkAI: true)) { // check if AI can win by one step
					} else if(Random.value < 0.5f && DoComboCheck (checkAI: false)) { // prevent Player's win 
					} else { DoRandomStep (); }

				break;
			case AIModel.AIDifficulty.Invincible:
				
				// if method returns true then AI made step
				if (DoComboCheck (checkAI: true)) { // check if AI can win by one step
				} else if(DoComboCheck (checkAI: false)) { // prevent Player's win 
				} else if(DoComplexComboCheck ()) { 
				} else { DoRandomStep (); }

				break;
		}
	}
	

	// Fill any free cell
	private void DoRandomStep() {
		List<Cell> listFreeCells = new List<Cell> ();
		for(int y=0; y<3; y++) {
			for(int x=0; x<3; x++) {
				if(game.model.gridCells[y,x].Value == 0) {
					listFreeCells.Add(game.model.gridCells[y,x]);
				}
			}
		}

		int randomInd = Random.Range (0, listFreeCells.Count);

		game.Notify (GameEvent.AI_CELL_SELECTED, listFreeCells [randomInd]);
	}

	// Prevent Player from complex win combinations and fill central cell
	private bool DoComplexComboCheck() {
		int centralCellValue = game.model.gridCells [1, 1].Value;

		if (centralCellValue == 0) { // fill central cell if it's free
			game.Notify(GameEvent.AI_CELL_SELECTED, game.model.gridCells [1, 1]);			
			return true;
		} else if(centralCellValue == 1 && game.model.matchSteps == 1) { // fill corner cell if central field is busy
			game.Notify(GameEvent.AI_CELL_SELECTED, game.model.gridCells [0, 0]);		
			return true;
		} else if(centralCellValue == 1 && game.model.matchSteps == 3) { // fill another corner cell to prevent winning combo
			game.Notify(GameEvent.AI_CELL_SELECTED, game.model.gridCells [2, 0]);		
			return true;
		}


		return false;
	}

	// Check rows, columns and giagonals for combinations where 1 free cell left to win
	// bool checkAi 
	// if true - check AI's combination where 1 cell left to win and do step
	// if false - check Player's combination where 1 cell left to win and prevent it
	private bool DoComboCheck(bool checkAI) {
		if (CheckRows (checkAI)) {
			return true;
		} else if (CheckColumns (checkAI)) {
			return true;
		} else if (CheckDiagonal1 (checkAI)) {
			return true;
		} else if (CheckDiagonal2 (checkAI)) {
			return true;
		}
		
		return false;
	}

	private bool CheckRows(bool checkAI) {
		int rowSum;
		Cell freeCell; // store free cell which can be last for combo
		for (int j=0; j<3; j++) { // check each row for combo
			rowSum = 0;
			freeCell = null;
			for (int i=0; i<3; i++) {
				int cellValue = game.model.gridCells[j,i].Value;
				rowSum += cellValue;
				
				if(cellValue == 0) {
					freeCell = game.model.gridCells[j,i];
				}
			}
			
			if(((rowSum == -2 && checkAI) || (rowSum == 2 && !checkAI)) && freeCell) {			
				game.Notify(GameEvent.AI_CELL_SELECTED, freeCell);			
				return true;
			}
		}
		
		return false;
	}
	
	private bool CheckColumns(bool checkAI) {
		int columnSum;
		Cell freeCell; // store free cell which can be last for combo
		for (int j=0; j<3; j++) { // check each column for combo
			columnSum = 0;
			freeCell = null;
			for (int i=0; i<3; i++) {
				int cellValue = game.model.gridCells[i,j].Value;
				columnSum += cellValue;
				
				if(cellValue == 0) {
					freeCell = game.model.gridCells[i,j];
				}
			}
			
			if(((columnSum == -2 && checkAI) || (columnSum == 2 && !checkAI)) && freeCell) {			
				game.Notify(GameEvent.AI_CELL_SELECTED, freeCell);			
				return true;
			}
		}
		
		return false;
	}
	
	private bool CheckDiagonal1(bool checkAI) {
		int diagonalSum = 0;
		Cell freeCell = null; // store free cell which can be last for combo
		for (int j=0; j<3; j++) { // check \ - diagonal for combo
			for (int i=0; i<3; i++) {
				if(i == j) {
					int cellValue = game.model.gridCells[i,j].Value;
					diagonalSum += cellValue;
					
					if(cellValue == 0) {
						freeCell = game.model.gridCells[i,j];
					}
				}
			}
			
			if(((diagonalSum == -2 && checkAI) || (diagonalSum == 2 && !checkAI)) && freeCell) {			
				game.Notify(GameEvent.AI_CELL_SELECTED, freeCell);			
				return true;
			}
		}
		
		return false;
	}
	
	private bool CheckDiagonal2(bool checkAI) {
		int diagonalSum = 0;
		Cell freeCell = null; // store free cell which can be last for combo
		for (int j=0; j<3; j++) { // check / - diagonal for combo
			for (int i=0; i<3; i++) {
				if(i + j == 2) {
					int cellValue = game.model.gridCells[i,j].Value;
					diagonalSum += cellValue;

					if(cellValue == 0) {
						freeCell = game.model.gridCells[i,j];
					}
				}
			}
			
			if(((diagonalSum == -2 && checkAI) || (diagonalSum == 2 && !checkAI)) && freeCell) {			
				game.Notify(GameEvent.AI_CELL_SELECTED, freeCell);			
				return true;
			}
		}
		
		return false;
	}
}
