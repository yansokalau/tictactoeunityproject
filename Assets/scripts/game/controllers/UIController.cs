﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIController : GameElement, INotifiable {

	public GameObject gameOverPanel;
	private Animator gameOverPanelAnimator;

	public Text gameOverResultTxt;

	public Text winCountTxt;
	public Text failCountTxt;
	public Text drawCountTxt;


	// Use this for initialization
	void Start () {
		gameOverPanel.SetActive (false);
		gameOverPanelAnimator = gameOverPanel.GetComponent<Animator> ();
	}
	
	public void OnNotification (string eventType, Object target, params object[] data)
	{
		Debug.Log (eventType);
		switch (eventType) {
			case GameEvent.PLAY:	
				gameOverPanel.SetActive (false);
				break;
			case GameEvent.GAME_OVER:	
				if(game.model.currentGameState == GameModel.GameState.PlayerWins) {
					gameOverResultTxt.text = "You win!";
				} else if(game.model.currentGameState == GameModel.GameState.AIWins) {
					gameOverResultTxt.text = "You lose!";
				} else if(game.model.currentGameState == GameModel.GameState.NoOneWins) {
					gameOverResultTxt.text = "No one wins!";
				}	

				UpdateScore();

				gameOverPanel.SetActive (true);
				gameOverPanelAnimator.Play ("show");
				break;
			case GameEvent.UPDATE_SCORE:
				UpdateScore();
				break;
		}
	}

	public void PlayPressed() {
		game.Notify (GameEvent.PLAY, this);
	}

	public void MenuPressed() {
		Application.LoadLevel("menu");
	}

	public void UpdateScore() {
		winCountTxt.text = game.model.winCount.ToString ();
		failCountTxt.text = game.model.failCount.ToString ();
		drawCountTxt.text = game.model.drawCount.ToString ();
	}
}
