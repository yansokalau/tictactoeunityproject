﻿using UnityEngine;
using System.Collections;

public class GameFieldController : GameElement, INotifiable {

	// Use this for initialization
	void Start () {
		Cell[] listCells = GameObject.FindObjectsOfType (typeof(Cell)) as Cell[];
		foreach (Cell cell in listCells) {
			game.model.gridCells[cell.CellIndY, cell.CellIndX] = cell;
		}

		game.model.LoadPlayerPrefs ();
		game.Notify (GameEvent.UPDATE_SCORE, this);

		Reset ();
	}
	
	// Update is called once per frame
	void Update () {
		switch (game.model.currentGameState) {
			case GameModel.GameState.AIStepDelay:
				game.model.stepDelayTimer -= Time.deltaTime;

				if(game.model.stepDelayTimer <= 0f) {
					game.model.currentGameState = GameModel.GameState.AIStep;
					game.aiController.DoStep();
				}
				break;
		}
	}

	public void Reset() {
		game.model.matchSteps = 0;

		for(int y=0; y<3; y++) {
			for(int x=0; x<3; x++) {
				game.model.gridCells[y,x].Reset ();
			}
		}

		if (game.model.currentGameState == GameModel.GameState.PlayerWins) {
			game.model.playerBeginFirst = true;
		} else if (game.model.currentGameState == GameModel.GameState.AIWins) {
			game.model.playerBeginFirst = false;
		} else if (game.model.currentGameState == GameModel.GameState.NoOneWins) {
			game.model.playerBeginFirst = !game.model.playerBeginFirst;
		} else {
			game.model.playerBeginFirst = Random.value < 0.5f;
		}

		if (game.model.playerBeginFirst) {
			game.model.currentGameState = GameModel.GameState.PlayerStep;
		} else {
			game.model.stepDelayTimer = game.model.stepDelay;
			game.model.currentGameState = GameModel.GameState.AIStepDelay;
		}


	}

	public void GameOver(GameModel.GameState state) {
		game.model.currentGameState = state;

		if (game.model.currentGameState == GameModel.GameState.PlayerWins) {
			game.model.winCount++;
		} else if (game.model.currentGameState == GameModel.GameState.AIWins) {
			game.model.failCount++;
		} else if (game.model.currentGameState == GameModel.GameState.NoOneWins) {
			game.model.drawCount++;
		}

		game.model.SavePlayerPrefs ();

		game.Notify(GameEvent.GAME_OVER, this);
	}

	public void OnNotification (string eventType, Object target, params object[] data)
	{

		Cell cell = null;

		if (target is Cell) {
			cell = (Cell) target;
		}

		switch (eventType) {
			case GameEvent.PLAY:
				Reset ();
				break;
			case GameEvent.PLAYER_CELL_SELECTED:		
				if(cell.IsFree && game.model.currentGameState == GameModel.GameState.PlayerStep) {
					cell.FillPlayer();
					game.model.matchSteps++;
					
					if(!CheckForWinOrDraw()) {
						game.model.stepDelayTimer = game.model.stepDelay;
						game.model.currentGameState = GameModel.GameState.AIStepDelay;
					}	
				}
				break;
			case GameEvent.AI_CELL_SELECTED:
				if(game.model.currentGameState == GameModel.GameState.AIStep) {
					cell.FillAI();
					game.model.matchSteps++;
					
					if(!CheckForWinOrDraw()) {
						game.model.currentGameState = GameModel.GameState.PlayerStep;
					}
				}
				break;
		}

		//LogGridState ();
	}

	private bool CheckForWinOrDraw() {

		// if sum of cells values in row, column or diagonal is equal 3 - then player wins, 
		// if it's equal -3 - then AI wins
		if (CheckRows ()) {
			return true;
		} else if (CheckColumns ()) {
			return true;
		} else if (CheckDiagonal1 ()) {
			return true;
		} else if (CheckDiagonal2 ()) {
			return true;
		} else if (CheckDraw ()) {
			return true;
		}

		return false;
	}

	private bool CheckRows() {
		int rowSum;
		Cell[] comboCells = new Cell[3]; // store cells which can be in win combo
		for (int j=0; j<3; j++) { // check each row for combo
			rowSum = 0;
			for (int i=0; i<3; i++) {
				comboCells[i] = game.model.gridCells[j,i];
				rowSum += game.model.gridCells[j,i].Value;
			}

			if(Mathf.Abs (rowSum) == 3) {
				HighlightWinCombo(comboCells);

				if(rowSum > 0) {
					GameOver (GameModel.GameState.PlayerWins);
				} else {
					GameOver (GameModel.GameState.AIWins);
				}

				return true;
			}
		}

		return false;
	}

	private bool CheckColumns() {
		int columnSum;
		Cell[] comboCells = new Cell[3]; // store cells which can be in win combo
		for (int j=0; j<3; j++) { // check each column for combo
			columnSum = 0;
			for (int i=0; i<3; i++) {
				comboCells[i] = game.model.gridCells[i,j];
				columnSum += game.model.gridCells[i,j].Value;
			}
			
			if(Mathf.Abs (columnSum) == 3) {
				HighlightWinCombo(comboCells);

				if(columnSum > 0) {
					GameOver (GameModel.GameState.PlayerWins);
				} else {
					GameOver (GameModel.GameState.AIWins);
				}	

				return true;
			}
		}
		
		return false;
	}

	private bool CheckDiagonal1() {
		int diagonalSum = 0;
		Cell[] comboCells = new Cell[3]; // store cells which can be in win combo
		for (int j=0; j<3; j++) { // check \ - diagonal for combo
			for (int i=0; i<3; i++) {
				if(i == j) {
					comboCells[i] = game.model.gridCells[i,j];
					diagonalSum += game.model.gridCells[i,j].Value;
				}
			}
			
			if(Mathf.Abs (diagonalSum) == 3) {
				HighlightWinCombo(comboCells);

				if(diagonalSum > 0) {
					GameOver (GameModel.GameState.PlayerWins);
				} else {
					GameOver (GameModel.GameState.AIWins);
				}

				return true;
			}
		}
		
		return false;
	}

	private bool CheckDiagonal2() {
		int diagonalSum = 0;
		Cell[] comboCells = new Cell[3]; // store cells which can be in win combo
		for (int j=0; j<3; j++) { // check / - diagonal for combo
			for (int i=0; i<3; i++) {
				if(i + j == 2) {
					comboCells[i] = game.model.gridCells[i,j];
					diagonalSum += game.model.gridCells[i,j].Value;
				}
			}
			
			if(Mathf.Abs (diagonalSum) == 3) {
				HighlightWinCombo(comboCells);

				if(diagonalSum > 0) {
					GameOver (GameModel.GameState.PlayerWins);
				} else {
					GameOver (GameModel.GameState.AIWins);
				}

				return true;
			}
		}
		
		return false;
	}

	// last check if all cells are filled without win combination
	private bool CheckDraw() {
		for (int j=0; j<3; j++) {
			for (int i=0; i<3; i++) {
				if(game.model.gridCells[j,i].Value == 0) {
					return false;
				}
			}
		}

		GameOver (GameModel.GameState.NoOneWins);

		return true;
	}

	private void HighlightWinCombo(Cell[] comboCells) {
		foreach (Cell cell in comboCells) {
			cell.Highlight();
		}
	}
	
	public void LogGridState() {
		string result = "";
		for(int y=0; y<3; y++) {
			for(int x=0; x<3; x++) {
				result += game.model.gridCells[y,x].Value+"\t";
			}
			result+="\n";
		}
		Debug.Log (result);
	}
}
