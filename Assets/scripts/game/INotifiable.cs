﻿using UnityEngine;
using System.Collections;

public interface INotifiable {
	void OnNotification (string eventType, Object target, params object[] data);
}
