﻿using UnityEngine;
using System.Collections;

public class GameElement : MonoBehaviour {

	public TicTacToeGame game { 
		get { 
			return GameObject.FindObjectOfType<TicTacToeGame>(); 
		}
	}

}
