﻿using UnityEngine;
using System.Collections;

public class Cell : GameElement {
	
	private SpriteRenderer icon;
	private SpriteRenderer bg;

	private float highlightCellTimer;
	private bool highlighCell; // highligh cell if it's part of win combo

	private Color iconColor;

	public int CellIndX {
		get;
		private set;
	}

	public int CellIndY {
		get;
		private set;
	}

	// Use this for initialization
	void Start () {
		icon = transform.FindChild ("icon").GetComponent<SpriteRenderer> ();
		bg = transform.FindChild ("bg").GetComponent<SpriteRenderer> ();

		CellIndX = int.Parse(name.Substring (5, 1));
		CellIndY = int.Parse(name.Substring (7, 1));
	}

	public void Reset() {
		highlighCell = false;
		highlightCellTimer = 0f;

		SetAlpha (bg, 0f);
		icon.sprite = null;
		icon.color = GameModel.DefaultColor;
	}

	void Update() {
		if (highlighCell) {
			highlightCellTimer += Time.deltaTime;

			// icon color transition
			icon.color = Color.Lerp(iconColor, GameModel.DefaultColor, highlightCellTimer);
			// show cell background
			SetAlpha (bg, highlightCellTimer*0.3f);

			if(highlightCellTimer >= 1f) {
				highlighCell = false;
			}
		}
	}

	public void FillPlayer() {
		if (game.model.playerBeginFirst) {
			icon.sprite = game.model.crossSprite;
		} else {
			icon.sprite = game.model.circleSprite;
		}

		icon.color = GameModel.PlayersColor;
	}

	public void FillAI() {
		if (game.model.playerBeginFirst) {
			icon.sprite = game.model.circleSprite;
		} else {
			icon.sprite = game.model.crossSprite;
		}
		
		icon.color = GameModel.AIColor;
	}

	// Highligh cell which is part of win combo
	public void Highlight() {
		iconColor = icon.color; // store color to make transition
		bg.color = icon.color;
		SetAlpha (bg, 0f);

		highlighCell = true;
	}

	void OnMouseDown() {
		game.Notify (GameEvent.PLAYER_CELL_SELECTED, this);
	}

	private void SetAlpha(SpriteRenderer sr, float a) {
		Color c = sr.color;
		c.a = a;
		sr.color = c;
	}

	// 0 - free cell, 1 - player's cell, -1 - ai's cell
	public int Value {
		get {
			if(icon.color == GameModel.PlayersColor) {
				return 1;
			} else if(icon.color == GameModel.AIColor) {
				return -1;
			}

			return 0;
		}
	}

	public bool IsFree {
		get { return Value == 0; }
	}
}
